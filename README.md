# Test Git

Des tests de git sur Codeberg

## Mais pourquoi ?

Chaque fois que je crée un nouveau repository sur *Codeberg* ou *GitHub* pour un nouveau projet pour lequel j'ai déjà un morceau de code et quelques répertoires, je me mélange les pinceaux sur les commandes `git` et surtout sur l'ordre des commandes.

Le résultat est toujours le même :

```
error: src refspec main does not match any
error: failed to push some refs to '<url du repository>'
```

Généralement suivit par un 

```
 ! [rejected]        main -> main (non-fast-forward)
error: failed to push some refs to '<url du repository>'
hint: Updates were rejected because the tip of your current branch is behind
hint: its remote counterpart. Integrate the remote changes (e.g.
hint: 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
```

## Tu as trouvé une solution ?

Oui. Et ça marche quasi à tous les coups.

Voilà la méthode.

### 1 - créer le repository sur la plateforme (ici, Codeberg)

Je ne vais pas non plus faire des screenshots, hein ? 

N'oubliez pas de spécifier une licence (**GPL-v2 only** pour ma part ... *What else?*) et de mettre votre repository en public (oui, je sais, tout ça se discute).

### 2 - initialiser votre repository git

Commencez par copier l'url du repository, ça va servir ici ! Préférez l'url SSH (du coup, n'oubliez pas d'ajouter votre clé SSH à la plateforme).

```
git init
git remote add origin <url du repository>
```

### 3 - on ajoute ses fichiers

Avant ça, vérifier votre fichier `.gitignore` pour éviter de commiter les caches, les répertoires de build, etc.

```
git add .
```

### 3 - faites un premier pull pour un merge initial

On se positionne sur la branche `main` puis on pull avec l'option `--allow-unrelated-histories`.

```
git checkout -b main
git pull origin main --allow-unrelated-histories
```

Là on peut avoir un conflit de merging, souvent avec le fichier `README.md`. Généralement, ça se limite à une dizaine de secondes de travail pour corriger le conflit.

N'oubliez qu'après avoir corrigé les fichiers en conflit qu'il faut les réajouter !

```
git add .
```

### 4 - on commit et on push

Là on est presque dans la situation normale, hors *pull request*. 

Il faudra ne pas oublier de spécifier l'*upstream* avec l'option `--set-upstream` pour le push.

```
git commit -m "my initial commit"
git push --set-upstream origin main
```

## Conclusion

Voilà, suffit de ... non, pas juste lire la doc mais expérimenter, se documenter, se planter ... et ça fini par marcher ... jusqu'à la prochaine connerie :)